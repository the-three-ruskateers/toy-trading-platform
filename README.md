Names:			Matthew Cooper, Chris Lu, Travis Noyes

Project Name:		Toy Trading Platform

Project Topic:		Stock trading simulation

Gitlab URL:			https://gitlab.cecs.pdx.edu/the-three-ruskateers/toy-trading-platform

# Specific Project Vision:

The main idea of the program is a simulated trading environment with a simple user interface, and multiple users accessing the same resources concurrently. We will create an interactive client program for the user, and a server to handle the back-end transactions. The server will maintain data of what’s being bought and sold through a sqlite database. The user will be able to login with desired resources/funds, view/create/purchase listings and view transactions. The project will have 2 main projects. A client CLI and a server built using Rocket framework, as well as, diesel ORM.

## Server

### Overview
* Designed to handle variable number of clients
* Will maintain state of the following information
    * User
    * Listings
    * Transactions
* API endpoints
    * Initialize new user with funds and products
    * View user resources
    * View listings
    * Create listings
    * Purchase listings
    * View transactions

### Architecture
* The server can be divided into 5 parts
    * Dtos - Minimal structs used to define requests and responses to/from the server endpoints.
    * Endpoints - Route definitions coupled with business logic that will save/query the database information.
    * Models - Used by the server to load or save true business objects but will not be serialized/deserialized to
               the outside world.
    * Database schema models - Used by the diesel ORM to define sqlite database definitions.
    * Db scripts - Used to run migrations on the database.

## Client UI
* The client will allow the user to use all the endpoints just listed.

## Stretch goals:
* Take in transaction request into a transaction queue
* Set up market simulator
    * Computer buys/sells on its own 
    * Load testing (how many users can our stuff handle concurrently)

# Discussion
## Concerns

* We don’t know the Rust UI story. We’ll need to explore Rust native client UI or Rust Web Assembly. We may have to make concessions depending on how mature the Rust UI frameworks are. In the worst case scenario, we may have to use a different language for the UI to meet our needs.
* Hopefully Rust’s concurrency model isn’t too different from other languages or too difficult to implement. It should be as simple as locking the shared resources when reading and writing.
* The first implementation of concurrency may not be efficient but we are hoping it can sustain 3 users for demonstration as MVP.
* There will be no user authentication or authorization.

# Environment Dependencies

`sqlite3`

  * **macOS:** `brew install sqlite`
  * **Debian**, **Ubuntu:** `apt-get install libsqlite3-dev`
  * **Arch:** `pacman -S sqlite`

`openSsh`

  * **macOS:** `brew install openssh`
  * **Debian**, **Ubuntu:** `apt-get install libssl-dev`
  * **Arch:** `pacman -S openssh`

## Works Cited
https://github.com/SergioBenitez/Rocket