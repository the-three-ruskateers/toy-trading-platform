use crate::frontend::helper::clear_screen;
use prompted::input;
use serde::Deserialize;

// Display all transactions
pub(crate) async fn get_all_transactions(
    my_user_id: i32,
) -> Result<(), Box<dyn std::error::Error>> {
    #[derive(Deserialize)]
    struct TransactionResponse {
        seller_id: i32,
        fruit_type: i32,
        quantity: i32,
        price_per: i32,
    }

    #[derive(Deserialize)]
    struct ReturnedTransactionResponse {
        transactions: Vec<TransactionResponse>,
    }

    let resp = reqwest::get(format!(
        "http://127.0.0.1:8000/user/{}/transactions",
        my_user_id
    ))
    .await?
    .json::<ReturnedTransactionResponse>()
    .await?;

    clear_screen();
    println!("Showing all transactions:\n");
    for element in resp.transactions {
        let fruit_type_string = match element.fruit_type {
            1 => "Apples",
            2 => "Bananas",
            3 => "Oranges",
            _ => "Undefined",
        };
        println!("\nSeller ID:\t{}", element.seller_id);
        println!("Type:\t\t{}", fruit_type_string);
        println!("Quantity:\t{}", element.quantity);
        println!("Price (each):\t${}", element.price_per);
    }

    input!("\nPress Enter to continue...");
    Ok(())
}
