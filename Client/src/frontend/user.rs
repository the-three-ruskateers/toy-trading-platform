use crate::frontend::helper::clear_screen;
use prompted::input;
use serde::Deserialize;
use std::collections::HashMap;

// Login
// Get username, # of fruits, and $ from user input
// POST it to the server at an endpoint to create a new user in the database
pub(crate) async fn create_new_user() -> Result<i32, Box<dyn std::error::Error>> {
    println!("= CREATE NEW USER\n");
    println!("= Please enter your information:");
    let mut apples = String::new();
    while apples.is_empty() {
        apples = input!("  # of Apples\t>  ");
    }
    let mut bananas = String::new();
    while bananas.is_empty() {
        bananas = input!("  # of Bananas\t>  ");
    }
    let mut oranges = String::new();
    while oranges.is_empty() {
        oranges = input!("  # of Oranges\t>  ");
    }
    let mut funds = String::new();
    while funds.is_empty() {
        funds = input!("  Funds\t\t> $");
    }

    // This will POST a body of {"username":"","apples":"", etc}
    let mut map = HashMap::new();
    map.insert("apples", apples.parse::<i32>().unwrap());
    map.insert("bananas", bananas.parse::<i32>().unwrap());
    map.insert("oranges", oranges.parse::<i32>().unwrap());
    map.insert("funds", funds.parse::<i32>().unwrap());

    #[derive(Deserialize)]
    struct UserCreatedResponse {
        id: i32,
    }
    let client = reqwest::Client::new();
    let res = client
        .post("http://127.0.0.1:8000/user")
        .json(&map)
        .send()
        .await?
        .json::<UserCreatedResponse>()
        .await?;

    println!("\nUser created successfully!");
    input!("Press Enter to continue...");
    Ok(res.id)
}

// Display User
// GET request the server endpoint to view the user's current $ and fruits
pub(crate) async fn display_user_information(
    my_user_id: i32,
) -> Result<(), Box<dyn std::error::Error>> {
    let url = format!("http://127.0.0.1:8000/user/{}", my_user_id);

    #[derive(Deserialize)]
    struct UserInfoResponse {
        apples: i32,
        bananas: i32,
        oranges: i32,
        funds: i32,
    }

    let resp = reqwest::get(url).await?.json::<UserInfoResponse>().await?;

    clear_screen();
    println!("Here is your current information:\n");
    println!("User ID:  {}", my_user_id);
    println!("Apples:\t  {}", resp.apples);
    println!("Bananas:  {}", resp.bananas);
    println!("Oranges:  {}", resp.oranges);
    println!("Funds: \t ${}", resp.funds);
    input!("\nPress Enter to continue...");
    Ok(())
}
