use crate::frontend::listings::{create_new_listing, get_all_listings, purchase_listing};
use crate::frontend::transactions::get_all_transactions;
use crate::frontend::user::{create_new_user, display_user_information};
use prompted::input;

// These functions are mainly to help the user navigate through menus
// and to guide the user interface of the client program

// Welcome User
pub(crate) async fn welcome() -> Result<i32, Box<dyn std::error::Error>> {
    clear_screen();
    println!("= Toy Trading Platform =================");
    println!("= Created by: Matthew, Chris, and Travis\n\n");
    input!("> Press the Enter key to begin...");
    clear_screen();
    create_new_user().await
}
// Menu Options
pub(crate) fn menu() {
    clear_screen();
    println!("\n\n= Main Menu =====================");
    println!("= 1\tDisplay User Information");
    println!("= 2\tCreate Listing");
    println!("= 3\tDisplay Listings");
    println!("= 4\tPurchase Listing");
    println!("= 5\tDisplay My Transactions");
    println!("= 9\tExit Program");
    println!("\n");
}

pub(crate) fn fruit_menu() {
    println!("\n\n= Choices =====================");
    println!("= 1\tApples");
    println!("= 2\tBananas");
    println!("= 3\tOranges");
    println!("\n");
}

// Get User Choice
pub(crate) fn get_choice() -> i32 {
    let mut user_input = String::new();
    while user_input.is_empty() {
        user_input = input!("\nChoose an option and press Enter   > ");
    }
    let choice: i32 = user_input.parse().unwrap();
    choice
}

// Navigate
pub(crate) async fn navigate(my_user_id: i32) -> Result<(), Box<dyn std::error::Error>> {
    let user_id = my_user_id;
    let mut c = 0;
    while c != 9 {
        menu();
        c = get_choice();
        match c {
            1 => display_user_information(user_id).await?,
            2 => create_new_listing(user_id).await?,
            3 => get_all_listings().await?,
            4 => purchase_listing(user_id).await?,
            5 => get_all_transactions(user_id).await?,
            9 => {
                clear_screen();
                println!("\n\n\n===== GOODBYE! =====\n\n\n");
            }
            _ => {
                println!("Invalid choice");
            }
        }
    }
    Ok(())
}

// Clear Screen
pub(crate) fn clear_screen() {
    let mut i = 0;
    while i < 50 {
        println!();
        i += 1;
    }
}
