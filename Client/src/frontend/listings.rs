use crate::frontend::helper::{clear_screen, fruit_menu, get_choice};
use prompted::input;
use serde::Deserialize;
use std::collections::HashMap;

// Create New Listing
// Get username, # of fruits, and $ from user input
// POST it to the server at an endpoint to create a new user in the database
// Returns the total number of listings
pub(crate) async fn create_new_listing(my_user_id: i32) -> Result<(), Box<dyn std::error::Error>> {
    clear_screen();
    println!("= CREATE NEW LISTING\n");
    println!("= What type of fruit are you selling?");
    fruit_menu();
    let mut fruit_type = 0;
    while !(1..=3).contains(&fruit_type) {
        fruit_type = get_choice();
    }
    let quantity: String = input!("  Quantity\t> ");
    let price_per: String = input!("  Price (each)\t> $");

    // This will POST a body of {"username":"","apples":"", etc}
    let mut map = HashMap::new();
    map.insert("seller_id", my_user_id);
    map.insert("fruit_type", fruit_type);
    map.insert("quantity", quantity.parse::<i32>().unwrap());
    map.insert("price_per", price_per.parse::<i32>().unwrap());

    #[derive(Deserialize)]
    struct ListingCreatedResponse {
        available_quantity_now: i32,
    }
    let client = reqwest::Client::new();
    let res = client
        .post("http://127.0.0.1:8000/listing")
        .json(&map)
        .send()
        .await?
        .json::<ListingCreatedResponse>()
        .await?;

    println!("\nListing created successfully!");
    println!("\n\nYou now have this many:");
    println!("{}", res.available_quantity_now);
    input!("\nPress Enter to continue...");
    Ok(())
}

// Display Listings
// GET request the server all of the current listings available
pub(crate) async fn get_all_listings() -> Result<(), Box<dyn std::error::Error>> {
    #[derive(Deserialize)]
    struct ListingResponse {
        id: i32,
        fruit_type: i32,
        quantity: i32,
        price_per: i32,
    }

    #[derive(Deserialize)]
    struct ReturnedListingResponse {
        listings: Vec<ListingResponse>,
    }

    let resp = reqwest::get("http://127.0.0.1:8000/listings")
        .await?
        .json::<ReturnedListingResponse>()
        .await?;

    clear_screen();
    println!("Showing all listings:\n");
    for element in resp.listings {
        let fruit_type_string = match element.fruit_type {
            1 => "Apples",
            2 => "Bananas",
            3 => "Oranges",
            _ => "Undefined",
        };
        println!("\nListing ID:\t{}", element.id);
        println!("Type:\t\t{}", fruit_type_string);
        println!("Quantity:\t{}", element.quantity);
        println!("Price (each):\t${}", element.price_per);
    }

    input!("\nPress Enter to continue...");
    Ok(())
}

// Purchase Listing
// POST request to give the listing id and buyer id in order to buy a listing
pub(crate) async fn purchase_listing(my_user_id: i32) -> Result<(), Box<dyn std::error::Error>> {
    clear_screen();
    get_all_listings().await?;
    println!("\n\n= PURCHASE LISTING\n");
    let c: i32 = get_choice();

    let mut map = HashMap::new();
    map.insert("buyer_id", my_user_id);
    map.insert("listing_id", c);

    #[derive(Deserialize)]
    struct PurchasedResponse {
        new_amount_of_funds_available: i32,
    }
    let client = reqwest::Client::new();
    let res = client
        .post("http://127.0.0.1:8000/purchase")
        .json(&map)
        .send()
        .await?
        .json::<PurchasedResponse>()
        .await?;

    println!("\nListing purchased successfully!");
    println!("\n\nYou now have this much money:");
    println!("{}", res.new_amount_of_funds_available);
    input!("\nPress Enter to continue...");
    Ok(())
}
