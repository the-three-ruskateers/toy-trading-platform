/// Authors: Travis Noyes, Chris Lu, Matthew Cooper
use crate::frontend::helper::{navigate, welcome};
mod frontend;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let user_id: i32 = welcome().await?;
    navigate(user_id).await?;
    Ok(())
}
