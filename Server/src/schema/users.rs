diesel::table! {
    users (id) {
        id -> Integer,
        apples -> Integer,
        oranges -> Integer,
        bananas -> Integer,
        funds -> Integer,
    }
}
