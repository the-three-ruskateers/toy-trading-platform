diesel::table! {
    transactions (id) {
        id -> Integer,
        buyer_id -> Integer,
        seller_id -> Integer,
        fruit_type -> Integer,
        quantity -> Integer,
        price_per -> Integer,
    }
}
