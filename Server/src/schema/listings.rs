diesel::table! {
    listings (id) {
        id -> Integer,
        seller_id -> Integer,
        fruit_type -> Integer,
        quantity -> Integer,
        price_per -> Integer,
        is_deleted -> Bool
    }
}
