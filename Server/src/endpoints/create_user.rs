use crate::dtos::create_user_request::CreateUserRequest;
use crate::dtos::create_user_response::CreateUserResponse;
use crate::models::user::UserNew;
use crate::schema::users::users;
use diesel::prelude::*;
use rocket::response::Debug;
use rocket::serde::json::Json;

type Result<T, E = Debug<diesel::result::Error>> = std::result::Result<T, E>;

use crate::endpoints::db_struct::Db;

// Create a user with the given resources/funds.
#[post("/user", data = "<create_user_request>")]
pub(crate) async fn create_user(
    db: Db,
    create_user_request: Json<CreateUserRequest>,
) -> Result<Json<CreateUserResponse>> {
    let user = UserNew {
        apples: create_user_request.apples,
        oranges: create_user_request.oranges,
        bananas: create_user_request.bananas,
        funds: create_user_request.funds,
    };

    let id_val = db
        .run(move |conn| {
            diesel::insert_into(users::table)
                .values(&user)
                .execute(conn)?;

            users::table
                .select(users::id)
                .order(users::id.desc())
                .first(conn)
        })
        .await?;

    let response = CreateUserResponse { id: id_val };
    Ok(Json(response))
}
