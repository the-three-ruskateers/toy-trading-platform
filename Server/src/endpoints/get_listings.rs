use crate::dtos::get_listings_response::*;
use crate::models::listing::Listing;
use crate::schema::listings::listings;
use diesel::prelude::*;
use rocket::response::Debug;
use rocket::serde::json::Json;

type Result<T, E = Debug<diesel::result::Error>> = std::result::Result<T, E>;

use crate::endpoints::db_struct::Db;

// Retrieve a list of all the listings.
#[get("/listings")]
pub(crate) async fn get_listings(db: Db) -> Result<Json<GetListingsResponse>> {
    let listings: Vec<Listing> = db
        .run(move |conn| {
            listings::table
                .select(Listing::as_select())
                .filter(listings::is_deleted.eq(false))
                .load(conn)
        })
        .await?;

    let responses = listings
        .iter()
        .map(|x| ListingResponse {
            id: x.id,
            fruit_type: x.fruit_type,
            quantity: x.quantity,
            price_per: x.price_per,
        })
        .collect();

    let transaction_response = GetListingsResponse {
        listings: responses,
    };

    Ok(Json(transaction_response))
}
