// Shared definition of the Db struct to be used between startup and all of the endpoints.
#[database("diesel")]
pub(crate) struct Db(diesel::SqliteConnection);
