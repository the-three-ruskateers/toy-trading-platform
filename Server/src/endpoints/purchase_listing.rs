use crate::dtos::{
    purchase_listing_request::PurchaseListingRequest,
    purchase_listing_response::PurchaseListingResponse,
};
use crate::models::listing::Listing;
use crate::models::transaction::*;
use crate::models::user::User;
use crate::schema::listings::listings;
use crate::schema::transactions::transactions;
use crate::schema::users::users;
use diesel::prelude::*;
use rocket::response::Debug;
use rocket::serde::json::Json;

type Result<T, E = Debug<diesel::result::Error>> = std::result::Result<T, E>;

use crate::endpoints::db_struct::Db;

// Allow a user to purchase a listing. The buyer will have their funds debited upon successful transaction.
#[post("/purchase", data = "<purchase_listing_request>")]
pub(crate) async fn purchase_listing(
    db: Db,
    purchase_listing_request: Json<PurchaseListingRequest>,
) -> Result<Json<PurchaseListingResponse>> {
    let new_funds = db
        .run(move |conn| {
            let mut buyer = users::table
                .select(User::as_select())
                .filter(users::id.eq(purchase_listing_request.buyer_id))
                .first(conn)?;

            let mut listing = listings::table
                .select(Listing::as_select())
                .filter(listings::id.eq(purchase_listing_request.listing_id))
                .first(conn)?;

            let mut seller = users::table
                .select(User::as_select())
                .filter(users::id.eq(listing.seller_id))
                .first(conn)?;

            let listing_amount = listing.price_per * listing.quantity;
            if buyer.funds < listing_amount {
                return Ok(buyer.funds);
            } else {
                buyer.funds -= listing_amount;
            }

            match listing.fruit_type {
                1 => {
                    buyer.apples += listing.quantity;
                }
                2 => {
                    buyer.bananas += listing.quantity;
                }
                3 => {
                    buyer.oranges += listing.quantity;
                }
                _ => {
                    return Ok(buyer.funds);
                }
            }

            listing.is_deleted = true;

            seller.funds += listing_amount;

            diesel::update(users::table)
                .filter(users::id.eq(buyer.id))
                .set(&buyer)
                .execute(conn)?;

            diesel::update(users::table)
                .filter(users::id.eq(seller.id))
                .set(&seller)
                .execute(conn)?;

            diesel::update(listings::table)
                .filter(listings::id.eq(listing.id))
                .set(&listing)
                .execute(conn)?;

            let transaction = TransactionNew {
                buyer_id: buyer.id,
                seller_id: listing.seller_id,
                fruit_type: listing.fruit_type,
                quantity: listing.quantity,
                price_per: listing.price_per,
            };

            diesel::insert_into(transactions::table)
                .values(&transaction)
                .execute(conn)?;

            Ok::<i32, Debug<diesel::result::Error>>(buyer.funds)
        })
        .await?;

    let response = PurchaseListingResponse {
        new_amount_of_funds_available: new_funds,
    };

    Ok(Json(response))
}
