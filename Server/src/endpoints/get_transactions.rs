use crate::dtos::get_transactions_response::GetTransactionsResponse;
use crate::dtos::get_transactions_response::TransactionResponse;
use crate::models::transaction::Transaction;
use crate::schema::transactions::transactions;
use diesel::prelude::*;
use rocket::response::Debug;
use rocket::serde::json::Json;

type Result<T, E = Debug<diesel::result::Error>> = std::result::Result<T, E>;

use crate::endpoints::db_struct::Db;

// View all the transactions that involve a user with the given `user_id`
#[get("/user/<user_id>/transactions")]
pub(crate) async fn get_transactions(
    db: Db,
    user_id: i32,
) -> Result<Json<GetTransactionsResponse>> {
    let transactions: Vec<Transaction> = db
        .run(move |conn| {
            transactions::table
                .select(Transaction::as_select())
                .filter(
                    transactions::seller_id
                        .eq(user_id)
                        .or(transactions::buyer_id.eq(user_id)),
                )
                .load(conn)
        })
        .await?;

    let responses = transactions
        .iter()
        .map(|x| TransactionResponse {
            buyer_id: x.id,
            seller_id: x.seller_id,
            fruit_type: x.fruit_type,
            quantity: x.quantity,
            price_per: x.price_per,
        })
        .collect();

    let transaction_response = GetTransactionsResponse {
        transactions: responses,
    };

    Ok(Json(transaction_response))
}
