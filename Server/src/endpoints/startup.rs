use rocket::fairing::AdHoc;
use rocket::{Build, Rocket};

use crate::endpoints::{
    create_listing::create_listing, create_user::create_user, db_struct::Db,
    get_listings::get_listings, get_transactions::get_transactions, get_user_info::get_user_info,
    purchase_listing::purchase_listing,
};

// Run the database migrations
async fn run_migrations(rocket: Rocket<Build>) -> Rocket<Build> {
    use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};

    const MIGRATIONS: EmbeddedMigrations = embed_migrations!("db/diesel/migrations");

    Db::get_one(&rocket)
        .await
        .expect("database connection")
        .run(|conn| {
            conn.run_pending_migrations(MIGRATIONS)
                .expect("diesel migrations");
        })
        .await;

    rocket
}

// Start the server api and define the endpoints.
pub(crate) fn stage() -> AdHoc {
    AdHoc::on_ignite("Diesel SQLite Stage", |rocket| async {
        rocket
            .attach(Db::fairing())
            .attach(AdHoc::on_ignite("Diesel Migrations", run_migrations))
            .mount(
                "/",
                routes![
                    create_user,
                    get_user_info,
                    create_listing,
                    get_listings,
                    purchase_listing,
                    get_transactions
                ],
            )
    })
}
