use crate::dtos::create_listing_request::CreateListingRequest;
use crate::dtos::create_listing_response::CreateListingResponse;
use crate::models::listing::ListingNew;
use crate::models::user::User;
use crate::schema::listings::listings;
use crate::schema::users::users;
use diesel::prelude::*;
use rocket::response::Debug;
use rocket::serde::json::Json;

type Result<T, E = Debug<diesel::result::Error>> = std::result::Result<T, E>;

use crate::endpoints::db_struct::Db;

// Create a listing and debit the resources from the user making the request.
#[post("/listing", data = "<create_listing_request>")]
pub(crate) async fn create_listing(
    db: Db,
    create_listing_request: Json<CreateListingRequest>,
) -> Result<Json<CreateListingResponse>> {
    let listing = ListingNew {
        seller_id: create_listing_request.seller_id,
        fruit_type: create_listing_request.fruit_type,
        quantity: create_listing_request.quantity,
        price_per: create_listing_request.price_per,
        is_deleted: false,
    };

    let (fruit_type, new_quantity) = db
        .run(move |conn| {
            diesel::insert_into(listings::table)
                .values(&listing)
                .execute(conn)?;

            let mut u = users::table
                .select(User::as_select())
                .filter(users::id.eq::<i32>(create_listing_request.seller_id))
                .first(conn)?;

            match create_listing_request.fruit_type {
                1 => {
                    u.apples -= create_listing_request.quantity;
                }
                2 => {
                    u.bananas -= create_listing_request.quantity;
                }
                3 => {
                    u.oranges -= create_listing_request.quantity;
                }
                _ => {}
            };

            diesel::update(users::table)
                .filter(users::id.eq(u.id))
                .set(&u)
                .execute(conn)?;

            match create_listing_request.fruit_type {
                1 => Ok::<(i32, i32), Debug<diesel::result::Error>>((
                    create_listing_request.fruit_type,
                    u.apples,
                )),
                2 => Ok::<(i32, i32), Debug<diesel::result::Error>>((
                    create_listing_request.fruit_type,
                    u.bananas,
                )),
                3 => Ok::<(i32, i32), Debug<diesel::result::Error>>((
                    create_listing_request.fruit_type,
                    u.oranges,
                )),
                _ => Ok::<(i32, i32), Debug<diesel::result::Error>>((0, 0)),
            }
        })
        .await?;

    let response = CreateListingResponse {
        fruit_type,
        available_quantity_now: new_quantity,
    };

    Ok(Json(response))
}
