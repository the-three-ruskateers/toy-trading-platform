use crate::dtos::get_user_info_response::GetUserInfoResponse;
use crate::models::user::User;
use crate::schema::users::users;
use diesel::prelude::*;
use rocket::response::Debug;
use rocket::serde::json::Json;

type Result<T, E = Debug<diesel::result::Error>> = std::result::Result<T, E>;

use crate::endpoints::db_struct::Db;

// Show the user their funds/resources
#[get("/user/<id>")]
pub(crate) async fn get_user_info(db: Db, id: i32) -> Result<Json<GetUserInfoResponse>> {
    let user: User = db
        .run(move |conn| {
            users::table
                .select(User::as_select())
                .filter(users::id.eq(id))
                .first(conn)
        })
        .await?;

    let response = GetUserInfoResponse {
        apples: user.apples,
        bananas: user.bananas,
        oranges: user.oranges,
        funds: user.funds,
    };

    Ok(Json(response))
}
