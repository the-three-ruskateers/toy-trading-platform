use rocket::serde::Serialize;

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
pub struct GetTransactionsResponse {
    pub transactions: Vec<TransactionResponse>,
}

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
pub struct TransactionResponse {
    pub buyer_id: i32,
    pub seller_id: i32,
    pub fruit_type: i32,
    pub quantity: i32,
    pub price_per: i32,
}
