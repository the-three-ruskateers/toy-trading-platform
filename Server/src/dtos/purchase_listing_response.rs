use rocket::serde::Serialize;

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
pub struct PurchaseListingResponse {
    pub new_amount_of_funds_available: i32,
}
