use rocket::serde::Deserialize;

#[derive(Deserialize)]
pub struct CreateListingRequest {
    pub seller_id: i32,
    pub fruit_type: i32,
    pub quantity: i32,
    pub price_per: i32,
}
