use rocket::serde::Deserialize;

#[derive(Deserialize)]
pub struct CreateUserRequest {
    pub apples: i32,
    pub bananas: i32,
    pub oranges: i32,
    pub funds: i32,
}
