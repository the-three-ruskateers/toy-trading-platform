use rocket::serde::Serialize;

#[derive(Serialize)]
pub struct GetListingsResponse {
    pub listings: Vec<ListingResponse>,
}

#[derive(Serialize)]
pub struct ListingResponse {
    pub id: i32,
    pub fruit_type: i32,
    pub quantity: i32,
    pub price_per: i32,
}
