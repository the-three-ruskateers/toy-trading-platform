use rocket::serde::Serialize;

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
pub struct CreateListingResponse {
    pub fruit_type: i32,
    pub available_quantity_now: i32,
}
