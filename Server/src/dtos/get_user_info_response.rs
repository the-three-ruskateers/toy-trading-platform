use rocket::serde::Serialize;

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
pub struct GetUserInfoResponse {
    pub apples: i32,
    pub bananas: i32,
    pub oranges: i32,
    pub funds: i32,
}
