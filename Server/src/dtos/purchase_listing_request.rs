use rocket::serde::Deserialize;

#[derive(Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct PurchaseListingRequest {
    pub buyer_id: i32,
    pub listing_id: i32,
}
