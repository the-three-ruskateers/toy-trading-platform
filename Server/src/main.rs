#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_sync_db_pools;

mod dtos;
mod endpoints;
mod models;
mod schema;

#[launch]
fn rocket() -> _ {
    rocket::build().attach(endpoints::startup::stage())
}
