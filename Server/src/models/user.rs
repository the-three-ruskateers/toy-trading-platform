use crate::schema::users::users;
use diesel::prelude::{AsChangeset, Identifiable, Insertable, Queryable, Selectable};

#[derive(Selectable, Debug, Clone, Copy, Queryable, Insertable, Identifiable, AsChangeset)]
#[diesel(table_name = users)]
pub struct User {
    pub id: i32,
    pub apples: i32,
    pub oranges: i32,
    pub bananas: i32,
    pub funds: i32,
}

#[derive(Debug, Clone, Copy, Insertable)]
#[diesel(table_name = users)]
pub struct UserNew {
    pub apples: i32,
    pub oranges: i32,
    pub bananas: i32,
    pub funds: i32,
}
