use crate::schema::transactions::transactions;
use diesel::prelude::{Insertable, Queryable, Selectable};

#[derive(Selectable, Debug, Clone, Copy, Queryable, Insertable)]
#[diesel(table_name = transactions)]
pub struct Transaction {
    pub id: i32,
    pub buyer_id: i32,
    pub seller_id: i32,
    pub fruit_type: i32,
    pub quantity: i32,
    pub price_per: i32,
}

#[derive(Debug, Clone, Copy, Insertable)]
#[diesel(table_name = transactions)]
pub struct TransactionNew {
    pub buyer_id: i32,
    pub seller_id: i32,
    pub fruit_type: i32,
    pub quantity: i32,
    pub price_per: i32,
}
