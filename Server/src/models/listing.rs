use crate::schema::listings::listings;
use diesel::prelude::{AsChangeset, Identifiable, Insertable, Queryable, Selectable};

#[derive(Selectable, Debug, Clone, Copy, Queryable, Insertable, AsChangeset, Identifiable)]
#[diesel(table_name = listings)]
pub struct Listing {
    pub id: i32,
    pub seller_id: i32,
    pub fruit_type: i32,
    pub quantity: i32,
    pub price_per: i32,
    pub is_deleted: bool,
}

#[derive(Debug, Clone, Copy, Insertable)]
#[diesel(table_name = listings)]
pub struct ListingNew {
    pub seller_id: i32,
    pub fruit_type: i32,
    pub quantity: i32,
    pub price_per: i32,
    pub is_deleted: bool,
}
