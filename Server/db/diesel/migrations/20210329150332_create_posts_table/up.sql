CREATE TABLE users (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    apples INTEGER,
    oranges INTEGER,
    bananas INTEGER,
    funds INTEGER
);

CREATE TABLE listings (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    seller_id INTEGER,
    fruit_type INTEGER,
    quantity INTEGER,
    price_per INTEGER,
    is_deleted BOOLEAN NOT NULL CHECK (is_deleted IN (0,1))
);

CREATE TABLE transactions (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    buyer_id INTEGER,
    seller_id INTEGER,
    fruit_type INTEGER,
    quantity INTEGER,
    price_per INTEGER
);